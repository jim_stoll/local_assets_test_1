/* Add navigation bar for the one theme */
var persNav = {},
    persNavPageNames = ["index", "about", "programs", "sign-up", "parents", "outreach", "volunteers", "stem-workforce"],
    persNavPageTitles = ["Home", "About", "Programs", "Sign Up!", "Parents", "Outreach", "Volunteers", "Stem Workforce"];

function getCookie(cname) {
    for (var name = cname + "=", ca = decodeURIComponent(document.cookie).split(";"), i = 0; i < ca.length; i++) {
        for (var c = ca[i];
            " " === c.charAt(0);) c = c.substring(1);
        if (0 === c.indexOf(name)) return c.substring(name.length, c.length)
    }
    return ""
}

function persNavOutput() {
    var windowURL = persNav.splitURL;
    if (-1 < windowURL.indexOf("en")) {
        var enIndex = windowURL.indexOf("en");
        windowURL = windowURL.splice(0, enIndex + 1)
    }
    return persNavPageNames.forEach(function(element, index) {
        "Edit" !== getCookie("cq-editor-layer.page") ? persNav.outputURLs.push(function(element) {
            var output = '<li class="vt-give-navItem"><a class="vt-give-navItem-link" href="';
            for (i = 0; i < windowURL.length; i++) 0 == i ? output += windowURL[i] + "//" : 1 == i ? output = output : output += windowURL[i] + "/";
            return output = output + element + '.html">'
        }(element) + persNavPageTitles[index] + "</a></li>") : 0 < window.location.href.indexOf("wcmmode=disabled") ? persNav.outputURLs.push(function(element) {
            var output = '<li class="vt-give-navItem"><a class="vt-give-navItem-link" href="';
            for (i = 0; i < windowURL.length; i++) 0 == i ? output += windowURL[i] + "//" : 1 == i ? output = output : output += windowURL[i] + "/";
            return output = output + element + '.html?wcmmode=disabled">'
        }(element) + persNavPageTitles[index] + "</a></li>") : persNav.outputURLs.push(function(element) {
            var output = '<li class="vt-give-navItem"><a class="vt-give-navItem-link" href="';
            for (i = 0; i < windowURL.length; i++) 0 == i ? output += windowURL[i] + "//" : 1 == i ? output = output : 2 == i ? output += windowURL[i] + "/editor.html/" : output += windowURL[i] + "/";
            return output = output + element + '.html">'
        }(element) + persNavPageTitles[index] + "</a></li>")
    }), persNav.outputURLs
}


persNav.splitURL = window.location.href.split("/"), persNav.outputURLs = [];
var giving = window.giving || {};

function playPause(id) {
    if (0 < $("#vt_bgVid").length) var myVideo = document.getElementById("vt_bgVid"),
        myVideoControl = "#vt_bgVid + .vt-vidPlayPause";
    if (0 < $(".vt-bgVid").length) myVideo = document.getElementById(id), myVideoControl = "#" + id + "+ .vt-vidPlayPause";
    myVideo.paused ? (myVideo.play(), $(myVideoControl).removeClass("vt-playButton")) : (myVideo.pause(), $(myVideoControl).addClass("vt-playButton"))
}

function generateVideoTag(config) {
    var video = $("<video></video>");
    for (var attr in config)
        if ("src" === attr) {
            var src = $("<source></source>");
            src.attr("src", config[attr]), src.attr("type", "video/mp4"), video.append(src)
        } else "autoplay" === attr && video.prop(attr), video.attr(attr, config[attr]);
    return video
}! function(g) {
    g.homeCTA = function(ctas) {
        0 < ctas.length && ctas.each(function() {
            var $this = $(this),
                bgImage = $this.find(".vt-callToAction-supplement").removeClass("col-lg-6").clone();
            $this.find(".vt-vtcontainer").append(bgImage), $this.find("> .row > .vt-callToAction-supplement").addClass("mobile"), $this.find(".vt-callToAction-message").removeClass("col-lg-6"), $this.find(".vt-callToAction-message-secondary-link").addClass("col-lg-4")
        })
    }, g.loadCTAVideo = function(ctas) {
        0 < ctas.length && window.videoConfig && (0 < window.location.href.indexOf("wcmmode=disabled") || "Edit" !== getCookie("cq-editor-layer.page")) && ctas.each(function() {
            if ($(this).find(".vt-callToAction-supplement-fig").css({
                    background: "rgba(215, 210, 203, 1)"
                }).hide(), 992 <= window.innerWidth) $(this).find(".vt-callToAction-supplement-fig").html(generateVideoTag(window.videoConfig)), $(this).find(".vt-callToAction-supplement-fig").append("<div class='vt-vidPlayPause' onclick='javascript:playPause();'></div>"), $(this).find(".vt-callToAction-supplement-fig").show();
            else {
                $(this).find(".vt-callToAction-supplement.mobile .vt-image").remove(), $(this).find(".vt-callToAction-supplement.mobile .vt-callToAction-supplement-fig-container").remove();
                var videoContainer = $("<div></div>").addClass("vt-callToAction-video").append(generateVideoTag(window.videoConfig));
                videoContainer.append("<div class='vt-vidPlayPause' onclick='javascript:playPause();'></div>"), $(this).find(".vt-callToAction-supplement.mobile").append(videoContainer)
            }
        })
    }
}(giving), $(document).ready(function() {
    if ($(".vt-page-path").prepend('<div class="vt-give-nav"> <div class="vt-give-logoWrapper"> <a class="vt-give-logoLink" href="https://give.vt.edu"><img class="vt-give-logo" src="/local_assets/www.give.vt.edu/images/campaign_logo_od.svg" focusable="false" aria-hidden="true" /><span class="vt-give-logoLink-text sr-only" >The Campaign for Virginia Tech</span></a> </div> <ul class="vt-give-navList"> ' + persNavOutput().join("") + " </ul> </div>"), 0 < $(".vt-article").length) {
        var pageSubtitle, pageTitle = $(".vt-page-info h1").text();
        0 < $(".vt-page-info .vt-page-subtitle").length ? (pageSubtitle = $(".vt-page-info .vt-page-subtitle").detach(), pageSubtitleText = pageSubtitle.html()) : pageSubtitleText = "", $(".vt-bodycol-content").prepend('<div class="parbase section text"><div class="vt-text vt-give-pageTitle vt-text-margins" data-emptytext="Text"><h1>' + pageTitle + '</h1></div> </div><div class="parbase section text"><div class="vt-text vt-give-subtitle vt-text-margins" data-emptytext="Text">' + pageSubtitleText + "</div></div>")
    }
    991 < window.innerWidth && 0 < $(".vt-article .vt-articleImage figure, .vt-vidHeader").length && (0 < window.location.href.indexOf("wcmmode=disabled") || "Edit" !== getCookie("cq-editor-layer.page")) && $(".vt-article #vt_with_rb, .vt-article #vt_no_rb").addClass("vt-give-uplift"), 991 < window.innerWidth && 0 < $(".vt-general .vt-articleImage figure, .vt-vidHeader").length && (0 < window.location.href.indexOf("wcmmode=disabled") || "Edit" !== getCookie("cq-editor-layer.page")) && $(".vt-general .vt-articleImage, .vt-general #vt_with_rb, .vt-general #vt_no_rb").addClass("vt-give-uplift"), $(".vt-giving-radioUI-radioInput").on("focus", function() {$(this).next().addClass("focus")}).on("blur", function() {$(this).next().removeClass("focus")});
    var ctas = $(".vt-home .vt-callToAction");
    giving.homeCTA(ctas.eq(0)), giving.loadCTAVideo(ctas.eq(0)), 0 < $(".vt-c-norb-margins").length && ($(".vt-c-norb-margins").addClass("mb-0"), $(".vt-c-norb-margins").parents(".vt-rightcol-content, .vt-nav-briefs").addClass("p-0"))
}), $(window).on("load", function() {}), $(window).resize(function() {});

$(document).ready(function() {
  $("#vt_one_content_area").removeClass("vt-give-uplift");
  var href = $(location).attr("href");
  /* Do not show breadcrumb if the last segment is index.html  */
  var last_segment=href.split('/').pop();
  if(last_segment == "index.html" || last_segment=="index.html?wcmmode=disabled"){
        $(".gateway").css("display", "none");
  }

  /* Highlight the active tab */
  $("[href='"+href+"']").css("color", "red");
    
   /* Hightlight the Program tab when the  child page shows */
  if(href.includes("programs") || href.includes("current-ktu-program") || href.includes("past-ktu-programs")){
      var programs = '';
      if(href.includes("current-ktu-program")){
         programs=href.replace("/current-ktu-program", "");
      }
      if(href.includes("past-ktu-programs")){
        programs=href.replace("/past-ktu-programs", "");
      }
      $("[href='"+programs+"']").css("color", "red");
      }
  
   else{$("a[href*='/programs']").removeAttr("style");} 

 });